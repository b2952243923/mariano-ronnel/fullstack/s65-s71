import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

const ReturnAndRefundPolicy = () => {
  return (
    <Container className="my-5">
      <h2>Return and Refund Policy</h2>
      <p>Last updated: [Date]</p>
      <p>
        Thank you for shopping at <strong>SariSari Na</strong>.
      </p>
      <p>
        Our products can be returned within <strong>[number of days, e.g., 30 days]</strong> of the purchase date. To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging.
      </p>
      <p>
        To initiate a return, please contact our customer support team at <strong>[customer support email]</strong> or <strong>[customer support phone number]</strong>. Please provide your order number and a detailed reason for the return.
      </p>
      <h4>Refunds</h4>
      <p>
        Once we receive your returned item, we will inspect it and notify you that we have received your returned item. We will immediately notify you on the status of your refund after inspecting the item.
      </p>
      <p>
        If your return is approved, we will initiate a refund to your original payment method. The time it takes for the refund to be reflected in your account may vary depending on your payment method and financial institution.
      </p>
      <h4>Shipping Costs</h4>
      <p>
        Please note that the original shipping and handling charges are non-refundable. If you received free shipping on your order, the cost of the shipping will be deducted from your refund.
      </p>
      <h4>Exchanges</h4>
      <p>
        If you wish to exchange an item for a different size, color, or a different product altogether, please contact our customer support team. Additional shipping charges may apply for exchanges.
      </p>
      <h4>Damaged or Defective Items</h4>
      <p>
        If your item arrives damaged or defective, please contact our customer support team within <strong>[number of days, e.g., 7 days]</strong> of receiving the item. We will arrange for a replacement or a refund for the damaged or defective item.
      </p>
      <h4>Non-Returnable Items</h4>
      <p>
        Certain types of items are non-returnable, such as <strong>[list any items that cannot be returned, e.g., perishable goods, personalized items]</strong>. Please contact our customer support team if you have any questions about whether an item is eligible for return.
      </p>
      <h4>Contact Us</h4>
      <p>
        If you have any questions or concerns regarding our Return and Refund Policy, please contact our customer support team at <strong>[customer support email]</strong> or <strong>[customer support phone number]</strong>.
      </p>
      <h4>Changes to this Policy</h4>
      <p>
        We reserve the right to update or change our Return and Refund Policy at any time. Any changes will be posted on this page with the revised date of the last update.
      </p>
      <p>
        By making a purchase from <strong>SariSari Na</strong>, you agree to the terms of this Return and Refund Policy.
      </p>
      <Row className="mt-4">
        <Col>
          <p><strong>SariSari Na</strong></p>
          <p><strong>123 Jp Rizal Street Galicia 12</strong></p>
          <p><strong>Manila, 4121</strong></p>
          <p><strong>Philippines</strong></p>
        </Col>
      </Row>
    </Container>
  );
};

export default ReturnAndRefundPolicy;
