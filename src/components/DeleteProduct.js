import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteProduct({ product, fetchData }) {
  const handleDelete = () => {
    // Send a DELETE request to the backend API to delete the product
    fetch(`${process.env.REACT_APP_API_URL}/products/${product}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.success) {
          // Show success message
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'Product deleted successfully',
          });
          // Refresh the product list after successful deletion
          fetchData();
        } else {
          // Show error message
          Swal.fire({
            title: 'Error!',
            icon: 'error',
            text: 'Failed to delete product',
          });
        }
      })
      .catch((error) => {
        console.error('Error deleting product:', error);
        // Show error message
        Swal.fire({
          title: 'Error!',
          icon: 'error',
          text: 'An error occurred while deleting the product',
        });
      });
  };

  return (
    <>
      <Button variant="danger" onClick={() => handleDelete()}>
        Delete
      </Button>
    </>
  );
}
