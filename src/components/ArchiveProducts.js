import Swal from 'sweetalert2';
import { Button } from 'react-bootstrap';

export default function ArchiveProducts({ product, isActive, fetchData }) {
    
    // Function for archive toggle
    const archiveToggle = (productId) => {
    

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'Product Successfully Updated',
          });
          fetchData();
        } else {
          Swal.fire({
            title: 'Error!',
            icon: 'error',
            text: 'Please try again',
          });
          fetchData();
        }
      });
  };

  const activateToggle = (productId) => {
    

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activateproduct`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'Product Successfully Updated'
          })
          fetchData();
        } else {
          Swal.fire({
            title: 'Error!',
            icon: 'error',
            text: 'Please try again'
          })
          fetchData();
        }
      });
  };

  return (
    <>
      {isActive ? (
        <Button className="btn btn-warning" onClick={() => archiveToggle(product)}>
          Archive
        </Button>
      ) : (
        
          <Button className="btn btn-success" onClick={() => activateToggle(product)}>
            Activate
          </Button>
        
      )}
    </>
  );
  
}
