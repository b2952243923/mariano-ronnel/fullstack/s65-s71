import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';

export default function OrderHistoryUser({ ordersData }) {
  return (
    <div>
    <h1 className='text-center pt-5 pb-5'>My Orders</h1>
      {Array.isArray(ordersData) && ordersData.length > 0 ? (
        <Row>
          {ordersData.map((order) => (
            <Col key={order._id} lg={6}>
              <Card className="mb-3">
                <Card.Body>
                  <Card.Title>Order ID: {order._id}</Card.Title>
                  <Card.Text>Total Amount: {order.totalAmount}</Card.Text>
                  <Card.Text>Ordered Date: {formatDate(order.createdOn)}</Card.Text>
                  {renderProductCards(order.products)}
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      ) : (
        <p>No orders found.</p>
      )}
    </div>
  );
}

function formatDate(dateString) {
  if (!dateString) {
    return '';
  }

  const date = new Date(dateString);

  if (isNaN(date.getTime())) {
    return '';
  }

  return date.toLocaleString();
}

function renderProductCards(products) {
  return products.map((product) => (
    <Card key={product.productId} className="mb-3">
      <Card.Body>
        {product.imgUrl && (
          <div className="image-container">
            <Card.Img
              src={product.imgUrl}
              alt={product.name}
              style={{ width: '100px', height: '100px', objectFit: 'cover' }}
            />
          </div>
        )}
        <Card.Title>{product.name}</Card.Title>
        <Card.Text>Product ID: {product.productId}</Card.Text>
        <Card.Text>Quantity: {product.quantity}</Card.Text>
      </Card.Body>
    </Card>
  ));
}
