// AdminOrderHistory.js
import React, { useState, useEffect } from 'react';
import { Card, Row, Col } from 'react-bootstrap';

export default function AdminOrderHistory() {
  // State to store all orders from different users
  const [allOrders, setAllOrders] = useState([]);

  // Fetch all orders from the backend (for Admin view)
  const fetchAllOrders = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });

      if (!response.ok) {
        throw new Error('Failed to fetch all orders');
      }

      const data = await response.json();
      setAllOrders(data);
    } catch (error) {
      console.error(error);
    }
  };

  // Fetch all orders upon the initial render of the component
  useEffect(() => {
    fetchAllOrders();
  }, []);

  return (
    <div>
      <h2 className='text-center pt-5 pb-5'>All Orders (Admin View)</h2>
      {Array.isArray(allOrders) && allOrders.length > 0 ? (
        <Row>
          {allOrders.map((order) => (
            <Col key={order._id} lg={6}>
              <Card className="mb-3">
                <Card.Body>
                  <Card.Title>Order ID: {order._id}</Card.Title>
                  <Card.Text>Total Amount: {order.totalAmount}</Card.Text>
                  <Card.Text>Ordered Date: {formatDate(order.createdOn)}</Card.Text>
                  <Card.Text>User ID: {order.userId}</Card.Text>
                  <Card.Text>Products:</Card.Text>
                  {renderProductCards(order.products)}
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      ) : (
        <p>No orders found.</p>
      )}
    </div>
  );
}

function formatDate(dateString) {
  if (!dateString) {
    return '';
  }

  const date = new Date(dateString);

  if (isNaN(date.getTime())) {
    return '';
  }

  return date.toLocaleString();
}

function renderProductCards(products) {
  return products.map((product) => (
    <Card key={product.productId} className="mb-3">
      <Card.Body>
        {product.imgUrl && (
          <div className="image-container">
            <Card.Img
              src={product.imgUrl}
              alt={product.name}
              style={{ width: '100px', height: '100px', objectFit: 'cover' }}
            />
          </div>
        )}
        <Card.Title>{product.name}</Card.Title>
        <Card.Text>Product ID: {product.productId}</Card.Text>
        <Card.Text>Quantity: {product.quantity}</Card.Text>
      </Card.Body>
    </Card>
  ));
}
