import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import './Testimonals.css'

const testimonialsData = [
  {
    id: 1,
    name: 'John Doe',
    review: 'Great products! I love the quality and the variety of options available. Highly recommended!  I\'ll definitely be back! The food was delicious and I really enjoyed the atmosphere',
    imgUrl: "https://i.pinimg.com/474x/91/15/0f/91150f74ed851a439b823893b6b75860.jpg"
  },
  {
    id: 2,
    name: 'Jane Smith',
    review: 'Excellent customer service. They were very responsive to my queries and helped me find the perfect product.  I\'ll definitely be back! The food was delicious and I really enjoyed the atmosphere',
    imgUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQghseGd78mtouioWk1Bd45houT0akafr6Djg&usqp=CAU"
  },

  {
    id: 3,
    name: 'Jane Doe',
    review: 'The staff was incredibly welcoming and helpful. I would definitely come back! The food was delicious and I really enjoyed the atmosphere. I\'ll definitely be back! ',
    imgUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBKl18ZAlCfwzFgwLbTJSIuHXn9o8gzXtEgA&usqp=CAU"
  }
  // Add more testimonials as needed
];

export default function Testimonials() {
    return (
      <Row className="mt-5 testimonials-container">
        <Col>
          <h2 className="text-center mb-4">Testimonials</h2>
          <Row className="justify-content-center">
            {testimonialsData.map((testimonial) => (
              <Col xs={12} md={6} lg={4} key={testimonial.id}>
                <Card className="testimonial-card p-3">
                  {testimonial.imgUrl && ( // Render the image if the imgUrl property exists
                    <div className="testimonial-image-container">
                      <img src={testimonial.imgUrl} alt={testimonial.name} className="testimonial-image" />
                    </div>
                  )}
                  <div className="testimonial-content">
                    <blockquote className="testimonial-review mb-0">
                      {testimonial.review}
                    </blockquote>
                  </div>
                  <div className="testimonial-footer">
                    <span className="testimonial-name">{testimonial.name}</span>
                    {/* Add additional CSS class for rating */}
                    <div className="testimonial-rating">
                      <span className="testimonial-rating-star">&#9733;</span>
                      <span className="testimonial-rating-text">5/5</span>
                    </div>
                  </div>
                </Card>
              </Col>
            ))}
          </Row>
        </Col>
      </Row>
    );
  }
  
