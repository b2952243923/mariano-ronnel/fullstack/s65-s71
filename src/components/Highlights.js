import { Row, Col, Card, Carousel } from 'react-bootstrap';

export default function Highlights() {
  const featuredProducts = [
    { id: 1, name: 'iPhone 20 Pro Max', imgUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRraVMlHsYdpa3St_HKCFmILTYfDgHtOx8-_g&usqp=CAU', price: 200 },
    { id: 2, name: 'macbook Air 100', imgUrl: 'https://www.macworld.com/wp-content/uploads/2023/07/MacBook-air-15in.jpg?quality=50&strip=all', price: 200 },
    { id: 3, name: 'HP Omen Paltos Gaming Laptop', imgUrl: 'https://www.pcworld.com/wp-content/uploads/2023/04/hp_omen_laptops_midair-100724990-orig.jpg?quality=50&strip=all', price: 200 },
    { id: 4, name: 'Mars Ware Scorpion Gaming Setup', imgUrl: 'https://mavigadget.com/wp-content/uploads/2022/08/3_41a25823-84b4-4c89-afa2-8ceb09a63766.jpg', price: 200 }
  ];

  // Function to shuffle the array
  const shuffledProducts = [
    featuredProducts.find((product) => product.id === 3),
    featuredProducts.find((product) => product.id === 1),
    featuredProducts.find((product) => product.id === 2),
    featuredProducts.find((product) => product.id === 4),
  ];

  return (
    <Row className='mt-3 mb-3'>
      <Col xs={12} md={4}>
        <Carousel className='carousel'>
          {shuffledProducts.map((product) => (
            <Carousel.Item key={product.id} className="carousel-item">
              <img
                className="d-block w-100"
                src={product.imgUrl}
                alt={product.name}
                style={{ height: '300px', objectFit: 'cover' }}
              />
              <Carousel.Caption className="carousel-caption" style={{ position: 'absolute', bottom: '0', left: '0', width: '100%', padding: '15px' }}>
                <div style={{ backgroundColor: 'rgba(0, 0, 0, 0.6)', color: '#fff', padding: '10px', borderRadius: '5px' }}>
                  <h3 style={{ marginBottom: '5px' }}>{product.name}</h3>
                  <p style={{ margin: '0' }}>Price: ${product.price}</p>
                </div>
              </Carousel.Caption>
            </Carousel.Item>
          ))}
        </Carousel>
      </Col>

      <Col xs={12} md={4}>
        <Carousel className='carousel'>
          {shuffledProducts.map((product) => (
            <Carousel.Item key={product.id} className="carousel-item">
              <img
                className="d-block w-100"
                src={product.imgUrl}
                alt={product.name}
                style={{ height: '300px', objectFit: 'cover' }}
              />
              <Carousel.Caption className="carousel-caption" style={{ position: 'absolute', bottom: '0', left: '0', width: '100%', padding: '15px' }}>
                <div style={{ backgroundColor: 'rgba(0, 0, 0, 0.6)', color: '#fff', padding: '10px', borderRadius: '5px' }}>
                  <h3 style={{ marginBottom: '5px' }}>{product.name}</h3>
                  <p style={{ margin: '0' }}>Price: ${product.price}</p>
                </div>
              </Carousel.Caption>
            </Carousel.Item>
          ))}
        </Carousel>
      </Col>

      <Col xs={12} md={4}>
        <Carousel className='carousel'>
          {shuffledProducts.map((product) => (
            <Carousel.Item key={product.id} className="carousel-item">
              <img
                className="d-block w-100"
                src={product.imgUrl}
                alt={product.name}
                style={{ height: '300px', objectFit: 'cover' }}
              />
              <Carousel.Caption className="carousel-caption" style={{ position: 'absolute', bottom: '0', left: '0', width: '100%', padding: '15px' }}>
                <div style={{ backgroundColor: 'rgba(0, 0, 0, 0.6)', color: '#fff', padding: '10px', borderRadius: '5px' }}>
                  <h3 style={{ marginBottom: '5px' }}>{product.name}</h3>
                  <p style={{ margin: '0' }}>Price: ${product.price}</p>
                </div>
              </Carousel.Caption>
            </Carousel.Item>
          ))}
        </Carousel>
      </Col>
    </Row>
  );
}
