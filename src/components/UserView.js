import { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ProductCard from './ProductCard';
import ProductSearch from './ProductSearch';

export default function UserView({ productsData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const activeProducts = productsData.filter(product => product.isActive === true);
    setProducts(activeProducts);
  }, [productsData]);

  return (
    <Container className="py-5">
      <h2 className="mb-4 text-center">Products</h2>
      <ProductSearch />

      <Row xs={1} md={3} className="g-4">
        {products.map(product => (
          <Col key={product._id}>
            <ProductCard productProp={product} />
          </Col>
        ))}
      </Row>
    </Container>
  );
}
