import React from 'react';
import { Card, Button, Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './ProductCard.css'

export default function ProductCard({ productProp }) {
  const { _id, name, description, price, imgUrl } = productProp;

  return (
    <Card id='productComponent1' className="p-3 h-100">
      {imgUrl && (
        <div className="image-container">
          <Card.Img src={imgUrl} alt={name} className="product-image" />
        </div>
      )}
      <Card.Body>
        <Container className="h-100">
          <Row className="h-100">
            <Col>
              <Card.Title className="mb-3">{name}</Card.Title>
              <Card.Subtitle className="mb-3">Description:</Card.Subtitle>
              <Card.Text className="mb-3">{description}</Card.Text>
              <Card.Subtitle className="mb-3">Price:</Card.Subtitle>
              <Card.Text className="mb-3">Php {price}</Card.Text>
              </Col>
              <Button className='view-details' as={Link} to={`/products/${_id}`} variant="primary">
                View Details
              </Button>
              
          </Row>
        </Container>
      </Card.Body>
    </Card>
  );
}
