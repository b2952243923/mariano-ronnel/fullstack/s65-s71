export const fetchCartItems = (userId) => {
    if (userId) {
      return fetch(`${process.env.REACT_APP_API_URL}/add-to-cart/getCartSubtotal`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log('Data received from API:', data);
          return data.itemsSubtotal;
        })
        .catch((error) => {
          console.error('Error fetching cart items:', error);
          return [];
        });
    }
    return Promise.resolve([]); // Return an empty array as cart items when userId is undefined
  };
  