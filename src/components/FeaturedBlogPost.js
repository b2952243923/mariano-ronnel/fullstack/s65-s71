import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import './FeaturedBlogPost.css'

const FeaturedBlogPosts = ({ featuredPosts }) => {
  return (
    <Row className="mt-4">
      {featuredPosts.map((post) => (
        <Col key={post.id} xs={12} md={4}>
          <Card className="mb-3">
            <Card.Img
              variant="top"
              src={post.image}
              style={{ height: '300px', objectFit: 'cover' }}
              className="img-hover"
            />
            <Card.Body>
              <Card.Title>{post.title}</Card.Title>
              <Card.Text>{post.excerpt}</Card.Text>
              <a href={post.link} className="btn btn-primary">
                Read More
              </a>
            </Card.Body>
          </Card>
        </Col>
      ))}
    </Row>
  );
};

export default FeaturedBlogPosts;
