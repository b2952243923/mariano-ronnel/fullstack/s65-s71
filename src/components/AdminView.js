import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table, Button } from 'react-bootstrap';
import { CSVLink } from 'react-csv';
import EditProduct from './EditProduct';
import ArchiveProducts from './ArchiveProducts';
import Swal from 'sweetalert2';
import DeleteProduct from './DeleteProduct'; // Import the DeleteProduct component


export default function AdminView({ productsData, fetchData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const productsArr = productsData.map((product) => (
      <tr key={product._id}>
        <td>{product._id}</td>
        <td>{product.name}</td>
        <td>{product.description}</td>
        <td>{product.price}</td>
        <td className={product.isActive ? 'text-success' : 'text-danger'}>
          {product.isActive ? 'Available' : 'Unavailable'}
        </td>
        <td>
          {/* Display product image here */}
          <img src={product.imgUrl} alt={product.name} style={{ width: '100px' }}/>
        </td>
        <td className='pt-4'>
          <EditProduct product={product._id} fetchData={fetchData} />
        </td>
        <td  className='pt-4'>
          <ArchiveProducts product={product._id} isActive={product.isActive} fetchData={fetchData} />
        </td>
        <td  className='pt-4'>
          <DeleteProduct product={product._id} fetchData={fetchData} />
        </td>
      </tr>
    ));

    setProducts(productsArr);
  }, [productsData]);

  const csvData = productsData.map((product) => ({
    ID: product._id,
    Name: product.name,
    Description: product.description,
    Price: product.price,
    Availability: product.isActive ? 'Available' : 'Unavailable',
  }));

  const handleExport = () => {
    Swal.fire({
      title: 'Exporting Data...',
      icon: 'info',
      showConfirmButton: false,
      timer: 1500,
    }).then(() => {
      Swal.close();
    });
  };

  return (
    <>
      <h1 className="text-center my-4">Admin Dashboard</h1>

      <div className="d-flex justify-content-center mt-4 pb-5">
        <Button variant="primary" className="mr-3 m-5" onClick={() => fetchData()}>
          Refresh Data
        </Button>
        <Link to="/addProduct">
          <Button variant="success" className="mr-3 m-5">
            Add Product
          </Button>
        </Link>
        <CSVLink
          data={csvData}
          filename={'products_data.csv'}
          className="btn btn-info m-5"
          onClick={handleExport}
        >
          Export Data
        </CSVLink>
      </div>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th className='text-center'>Image</th> 
            <th colSpan="2" className='text-center'>Actions</th>
            <th className='text-center'>Delete</th>
          </tr>
        </thead>
        <tbody>{products}</tbody>
      </Table>
      
    </>
  );
}
