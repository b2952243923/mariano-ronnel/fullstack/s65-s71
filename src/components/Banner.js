import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Banner.css'

export default function Banner ({data}) {
  const { title, content, destination, label} = data;

  return (
    <Row>
      <Col className="p-1 text-center banner">
        <h1>{title}</h1>
        <p>{content}</p>
        <Link className='btn btn-primary' to={destination}>{label}</Link>
      </Col>
    </Row>
  );
}