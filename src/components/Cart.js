import React from 'react';
import { ListGroup, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';


const Cart = ({ cartItems = [], removeFromCart, updateQuantity }) => {

  const navigate = useNavigate();

  
  const handleQuantityChange = (item, event) => {
    const newQuantity = parseInt(event.target.value, 10);
    updateQuantity(item.productId, newQuantity);
  };

  const calculateTotalPrice = () => {
    return cartItems.reduce((total, item) => total + item.itemSubtotal, 0);
  };

  const handleRemoveItem = (productId) => {
    Swal.fire({
      title: 'Remove Item',
      text: 'Are you sure you want to remove this item from the cart?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, remove it!',
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
    }).then((result) => {
      if (result.isConfirmed) {
        removeFromCart(productId);
        // Redirect to the product page after removing the item
        navigate('/product');
      }
    });
  };
  

  const handleProceedToCheckout = () => {
    // Implement your actual checkout logic here.
    // For example, you could show a success message and clear the cart.
    Swal.fire({
      title: 'Checkout',
      text: 'Proceeding to checkout...',
      icon: 'success',
      showConfirmButton: false,
      timer: 1500,
    }).then(() => {
      clearCart();
    });
  };

  const clearCart = () => {
    // Clear the cart by removing all items
    cartItems.forEach((item) => removeFromCart(item.productId));
  };

  return (
    <div>
      {cartItems.length === 0 ? (
        <p></p>
      ) : (
        <>
          <ListGroup>
            {cartItems.map((item) => (
              <ListGroup.Item key={item.productId}>
                <div className="d-flex justify-content-between align-items-center">
                  <span>
                    {item.productName} - Quantity:
                    <input
                      type="number"
                      min="1"
                      value={item.quantity}
                      onChange={(e) => handleQuantityChange(item, e)}
                    />
                  </span>
                  <Button variant="danger" onClick={() => handleRemoveItem(item.productId)}>
                    Remove
                  </Button>
                </div>
              </ListGroup.Item>
            ))}
          </ListGroup>
          <p>Total Price: PhP {calculateTotalPrice()}</p>
          <Button variant="success" onClick={handleProceedToCheckout}>
            Proceed to Checkout
          </Button>
        </>
      )}
    </div>
  );
};

export default Cart;
