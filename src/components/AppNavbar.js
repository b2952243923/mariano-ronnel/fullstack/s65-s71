// Old practice of importing components
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';


// New practice in importing components/modules.
// We destructure our components to import them separately
import { useContext} from 'react';
import {Navbar, Nav, Container, Image} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

	// const [user, setUser] = useState(localStorage.getItem("token"));

	const { user } = useContext(UserContext);

	console.log(user);


	return (

		<Navbar expand="lg" variant='light' bg='light' className="bg-dark sticky-top ">
		      <Container> 
		        <Navbar.Brand className='text-light' as={Link} to="/home" ><Image
            src="/images/SariSariNaLogo.png" // Update the path to your logo image
            alt="SariSari Na"
            height="40" // Adjust the height as needed
            className="d-inline-block align-top rounded-circle"
          /> SariSari Na</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav " className='bg-light'/>
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		            <Nav.Link  as={NavLink} to="/home" className='text-light'>Home</Nav.Link>
					<Nav.Link as={NavLink} to="/product" className='text-light'>Products</Nav.Link>
		            

		            {
		              	(user.id !== null) ?
		              	
		              		user.isAdmin 
          					?
          					<>
							  
          						<Nav.Link as={Link} to="/addProduct" className='text-light'>Add Product</Nav.Link>
								  <Nav.Link as={Link} to="/orders" className='text-light'>Orders</Nav.Link>
          						<Nav.Link as={Link} to="/logout" className='text-light' >Logout</Nav.Link>
          					</>
          					:
          					<>
							 
							  <Nav.Link as={Link} to="/cart" className="text-light">Cart</Nav.Link>
          						<Nav.Link as={Link} to="/profile" className='text-light'>Profile</Nav.Link>
								  <Nav.Link as={Link} to="/orders" className='text-light'>Orders</Nav.Link>
          						<Nav.Link as={Link} to="/logout" className='text-light'>Logout</Nav.Link>
          					</>
		              	:
			              	<>
				              	<Nav.Link as={NavLink} to="/login" className='text-light'>Login</Nav.Link>
				              	<Nav.Link as={NavLink} to="/register" className='text-light'>Register</Nav.Link>
				            </>
		            }

		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>

	)

};