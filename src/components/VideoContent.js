import React from 'react';
import { Row, Col } from 'react-bootstrap';

const VideoContent = () => {
  return (
    <div className="video-content">
      <h2 className="text-center pb-5">Featured Video</h2>
      <div className="video-wrapper">
        <Row>
          <Col xs={12} sm={4} md={4}>
            <iframe
              width="100%"
              height="315"
              src="https://www.youtube.com/embed/jivZf41PlJU"
              title="YouTube video player"
              frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              allowfullscreen
            ></iframe>
          </Col>
          <Col xs={12} sm={4} md={4}>
            <iframe
              width="100%"
              height="315"
              src="https://www.youtube.com/embed/kuOQdo7t3Xk"
              title="YouTube video player"
              frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              allowfullscreen
            ></iframe>
          </Col>
          <Col xs={12} sm={4} md={4}>
            <iframe
              width="100%"
              height="315"
              src="https://www.youtube.com/embed/RQOpTsT_Svs"
              title="YouTube video player"
              frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              allowfullscreen
            ></iframe>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default VideoContent;
