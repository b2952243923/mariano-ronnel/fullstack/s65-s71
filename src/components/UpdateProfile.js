import React, { useState } from 'react';
import Swal from 'sweetalert2';

const Profile = ({fetchUserProfile})=> {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  


  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === 'firstName') {
      setFirstName(value);
    } else if (name === 'lastName') {
      setLastName(value);
    } 
  };

  const handleUpdateProfile = () => {
    // Get the token from localStorage or wherever it's stored
    const token = localStorage.getItem('token');
  
    // Assuming you have a backend endpoint to update the profile
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`, // Add the token in the headers
      },
      body: JSON.stringify({ firstName, lastName }), // Use object shorthand
    })
      .then((response) => response.json())
      .then((data) => {
        // Handle success or display an error message
        console.log(data);
  
        // Show a success confirmation using SweetAlert2
        Swal.fire({
          icon: 'success',
          title: 'Profile Updated!',
          text: 'Your profile has been successfully updated.',
        });
        fetchUserProfile();
  
       
      })
      .catch((error) => {
        // Handle error
        console.error('Error updating profile:', error);
  
        // Show an error message using SweetAlert2
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Failed to update profile. Please try again later.',
        })
        fetchUserProfile();
        
      });
      
  };
  

  return (
    <div className="container">
      <h1 className='pt-5 pb-5'>Update Profile</h1>
      <div className="form-group pb-3">
        <label>First Name:</label>
        <input
          type="text"
          name="firstName"
          className="form-control"
          value={firstName}
          onChange={handleChange}
        />
      </div>
      <div className="form-group pb-3">
        <label>Last Name:</label>
        <input
          type="text"
          name="lastName"
          className="form-control"
          value={lastName}
          onChange={handleChange}
        />
      </div>
      <button className="btn btn-primary" onClick={handleUpdateProfile}>
        Update Profile
      </button>
      <br />
      <br />
    </div>
  );
};

export default Profile;
