import { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';




export default function EditProduct({product, fetchData}) {

	const [ productId, setProductId ] = useState("");
	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price, setPrice ] = useState("");
	const [imgUrl, setImgUrl] = useState("");
	const [ showEdit, setShowEdit ] = useState(false);

	const closeEditWithoutSave = () => {
		setShowEdit(false);
	  };

	const openEdit = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImgUrl(data.imgUrl);
		})

		// Then open the modal
		setShowEdit(true)
	}

	// function for closing the modal
	const closeEdit = () => {
		setShowEdit(false);
		setName("");
		setDescription("");
		setPrice("");
		setImgUrl("");
	}


	
	const editProduct = (e, productId) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				imgUrl: imgUrl
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product Successfully Updated'
				})
				closeEdit();
                fetchData();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
                closeEdit();
                fetchData();
			}
		})
	}

	return (
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit(product)}>Edit</Button>

		{/*Edit Modal*/}
			<Modal show={showEdit} onHide={closeEditWithoutSave}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
					    <Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productname">
						  <Form.Label>Name</Form.Label>
						  <Form.Control 
							  type="text"
							  required
							  value={name}
						  	  onChange={e => {setName(e.target.value)}}
						  />
						</Form.Group>

						<Form.Group controlId="productDescription">
						  <Form.Label>Description</Form.Label>
						  <Form.Control 
							  type="text"
							  required
							  value={description}
						  	  onChange={e => {setDescription(e.target.value)}}
						  />
						</Form.Group>

						<Form.Group controlId="productPrice">
						  <Form.Label>Price</Form.Label>
						  <Form.Control 
							  type="number"
							  required
							  value={price}
						  	  onChange={e => {setPrice(e.target.value)}}
						  />
						</Form.Group>

						<Form.Group controlId="productImgUrl">
						  <Form.Label>Image Url</Form.Label>
						  <Form.Control 
							  type="text"
							  required
							  value={imgUrl}
						  	  onChange={e => {setImgUrl(e.target.value)}}
						  />
						</Form.Group>

					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEditWithoutSave} >Close</Button>
						<Button variant="success" type="submit">Submit</Button>

					</Modal.Footer>
				</Form>
			</Modal>	
		</>

	)
}