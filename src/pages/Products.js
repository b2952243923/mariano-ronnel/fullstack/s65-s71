
import {useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
// import SearchProductByPrice from '../components/SearchProductByPrice';



export default function Products() {

	const { user } = useContext(UserContext);

	// State that will be used to store product retrieved from the database.
	const [products, setProducts] = useState([])

	

	// Create a function to fetch all products
	const fetchData = () => {
		
		// get all active products
		fetch(`${process.env.REACT_APP_API_URL}/products/retrieveallproducts`)
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			
			setProducts(data);
		});

	}


	// Retrieves the products from the databasee upon initial render of the "products" component
	useEffect(() =>{

		fetchData();

	},[]);

	return (
	    <>
		
	      {user.isAdmin ? (
	        
	        <AdminView productsData={products} fetchData={fetchData}/>
	      ) : (
	      
			<>
			{/* <SearchProductByPrice/> */}
	        <UserView productsData={products} />
			</>
			
	      )}
	    </>
		
	  );

};