// OrderHistoryPage.js
import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import OrderHistoryUser from '../components/OrderHistoryUser';
import AdminOrderHistory from '../components/AdminOrderHistory'; // Import the AdminOrderHistory component

export default function OrderHistoryPage() {
  const { user } = useContext(UserContext);

  // State to store user's orders
  const [userOrders, setUserOrders] = useState([]);

  // Fetch user's orders from the backend
  const fetchUserOrders = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/user-orders`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });

      if (!response.ok) {
        throw new Error('Failed to fetch user orders');
      }

      const data = await response.json();
      setUserOrders(data.orders);
    } catch (error) {
      console.error(error);
    }
  };

  // Fetch user's orders upon the initial render of the component
  useEffect(() => {
    fetchUserOrders();
  }, []);

  return (
    <div>
      {user.isAdmin ? (
        <AdminOrderHistory /> // Show AdminOrderHistory for Admin
      ) : (
        <OrderHistoryUser ordersData={userOrders} /> // Show OrderHistoryUser for regular users
      )}
    </div>
  );
}
