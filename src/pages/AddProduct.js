import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function AddProduct() {
  const { user } = useContext(UserContext);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [imgUrl, setImgUrl] = useState('');
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();

  function addProduct(e) {
    e.preventDefault();
    const token = localStorage.getItem('token');

    fetch(`${process.env.REACT_APP_API_URL}/products/createproduct`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        imgUrl: imgUrl
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Product Created Successfully',
            icon: 'success',
            text: 'You have successfully added this product.',
            timer: 1500,
            showConfirmButton: false
          });

          navigate('/product');
        } else {
          Swal.fire({
            title: 'Unsuccessful Product Creation',
            icon: 'error',
            text: 'Something went wrong. Please try again.'
          });
        }

        setName('');
        setDescription('');
        setPrice('');
        setImgUrl('');
      });
  }

  useEffect(() => {
    setIsActive(name !== '' && description !== '' && price !== '' && imgUrl !== '');
  }, [name, description, price, imgUrl]);

  return (
    <>
      {user.isAdmin === true ? (
        <Container>
          <Row className="justify-content-center">
            <Col xs={12} md={8} lg={6}>
              <Form onSubmit={addProduct} className="my-5">
                <h1 className="text-center mb-4">Add Product</h1>

                <Form.Group controlId="Name">
                  <Form.Label>Name:</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Name"
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="Description" className='pt-3'>
                  <Form.Label>Description:</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Description"
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="Price"  className='pt-3'>
                  <Form.Label>Price:</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Price"
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="Image Url"  className='pt-3'>
                  <Form.Label>Image Url:</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Image Url"
                    value={imgUrl}
                    onChange={e => setImgUrl(e.target.value)}
                    required
                  />
                </Form.Group>
                <div className="d-grid pt-5">
                  <Button variant="dark" type="submit" disabled={!isActive}>
                    Submit
                  </Button>
                </div>
              </Form>
            </Col>
          </Row>
        </Container>
      ) : (
        <Navigate to="/product" />
      )}
    </>
  );
}
