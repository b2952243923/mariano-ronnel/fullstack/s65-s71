import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
// import FeaturedProducts from "../components/FeaturedProducts";
import Testimonials from "../components/Testimonals";
import VideoContent from "../components/VideoContent";
import FeaturedBlogPosts from "../components/FeaturedBlogPost";
import { Row, Col, Container } from "react-bootstrap";
import ReturnAndRefundPolicy from "../components/ReturnAndRefund";


export default function Home() {

    const data ={
    title: 'Welcome my friend!',
    content:  '',
    destination: "/product",
    label: 'Order Now!'
    }

    const featuredPosts = [
        {
          id: 1,
          title: 'Wood grain texture wallpaper for iPhone',
          excerpt: 'In today\'s fast-paced digital world, finding moments of tranquility and connection with nature can be challenging. Fortunately, our smartphones offer a way to carry a piece of the natural world with us at all times. Among the myriad of customization options available, these wood grain texture wallpapers for iPhone stand out as a timeless and alluring choice, bringing warmth, sophistication, and a sense of calm to our busy lives.',
          image: 'https://media.idownloadblog.com/wp-content/uploads/2023/07/Wood-grain-texture-wallpaper-idownloadblog-mockup-1500x983.png',
          link: 'https://www.idownloadblog.com/',
        },
        {
          id: 2,
          title: 'Display Notifications on a Galaxy Watch Running Wear OS Powered by Samsung',
          excerpt: 'Creating notifications on Android wearables is very simple, but for security or user experience reasons, manufacturers can have varying policies related to notifications. When developing your application, you must take these policies into account when implementing notifications for the real devices that the application supports.',
          image: 'https://d3unf4s5rp9dfh.cloudfront.net/SDP_blog/2023-06-01-01-card.jpg',
          link: 'https://developer.samsung.com/sdp/blog/en-us/2023/06/01/display-notifications-on-a-galaxy-watch-running-wear-os-powered-by-samsung',
        },
        {
            id: 1,
            title: 'New HP Pro x360 Fortis designed for working and learning at home, school and anywhere else',
            excerpt: 'The newest addition to the HP Fortis PC portfolio is aimed at helping students and workers collaborate better, no matter where they are, with a durable and powerful Windows 11 laptop Recognizing that working and learning on-the-go may result in accidental bumps and falls, this PC has gone through military-grade testing to withstand drops, dust and tumbles.',
            image: 'https://blogs.windows.com/wp-content/uploads/prod/sites/2/2023/02/HP-Pro-x360-Fortis_HERO-800x600.jpg',
            link: 'https://blogs.windows.com/windowsexperience/2023/02/02/new-hp-pro-x360-fortis-designed-for-working-and-learning-at-home-school-and-anywhere-else/',
          }
       
      ];

      const footerContent = (
        <footer className="bg-dark text-light text-center pt-2">
          <Container>
            <Row>
              <Col>
                <p>&copy; {new Date().getFullYear()} SariSari Na. All rights reserved.</p>
                <p>
                  Made <span className="text-danger">&#x1F494;</span> by Ronnel Aldrin Mariano
                </p>
              </Col>
            </Row>
          </Container>
        </footer>
      );
    

    return (
        <>
          <Container fluid>
      {/* Banner Component */}
      <Row className="mt-3 mb-3 pt-5">
        <Col>
          <Banner data={data} />
        </Col>
      </Row>

      {/* Highlights Component */}
      <Row className="mt-3 mb-3">
        <Col>
          <Highlights />
        </Col>
      </Row>

      {/* Featured Blog Posts Component */}
      <Row className="mt-3 mb-3">
        <Col>
          <FeaturedBlogPosts featuredPosts={featuredPosts} />
        </Col>
      </Row>

      {/* Video Content Component */}
      <Row className="mt-3 mb-3">
        <Col>
          <VideoContent />
        </Col>
      </Row>
      

      {/* Testimonials Component */}
      <Row className="mt-3 mb-3">
        <Col>
          <Testimonials />
        </Col>
      </Row>
    </Container>

    <Row>
        <Col>
          <ReturnAndRefundPolicy/>
        </Col>
      </Row>
      
    {footerContent}
            
        </>
    )
}