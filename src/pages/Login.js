import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Container, Row, Col, Alert } from 'react-bootstrap';

export default function Login() {

	// Allows us to consume the User context object and its properties to be used for user validation
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password);

	function authenticateUser(e) {
		e.preventDefault();
	  
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
		  method: "POST",
		  headers: {
			"Content-Type": "application/json",
		  },
		  body: JSON.stringify({
			email: email,
			password: password,
		  }),
		})
		  .then((res) => res.json())
		  .then((data) => {
			console.log(data);
	  
			if (data.access) {
			  // Set the token of the authenticated user in the local storage.
			  localStorage.setItem("token", data.access);
	  
			  // Invoke the function for retrieving the user details
			  retrieveUserDetails(data.access);
	  
			  // Show login success message
			  Swal.fire({
				title: "Login Successful",
				icon: "success",
				text: "Welcome to SariSari Na!",
			  }).then(() => {
				// Reload the website after the success message is closed (after user acknowledges the message)
				window.location.reload();
			  });
			} else {
			  // Show login error message
			  Swal.fire({
				title: "Authentication Failed",
				icon: "error",
				text: "Check your login details and try again.",
			  });
			}
			setEmail("");
			setPassword("");
		  });
	  }

		const retrieveUserDetails = (token) => {

			// The token will be sent as part of the requests header information
			// We put "Bearer" in front of the token to follow the implementation standards for JWTs.
			fetch(`${process.env.REACT_APP_API_URL}/users/userdetails`, {
				headers:{
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation accross the whole application.
				setUser({
					id:data._id,
					isAdmin: data.isAdmin
				})
			})
		}

		useEffect(() => {

		if(email !== "" && password !== "") {

			setIsActive(true)
		} else {
			setIsActive(false)
		}


	}, [email,password]);

	return(

		<Container className="mt-5">
      <Row className="justify-content-center">
        <Col xs={12} md={6}>
          {(user.id !== null) ? <Navigate to="/home" /> : (
            <Form onSubmit={e => authenticateUser(e)}>
              <h1 className="my-5 text-center">Login</h1>

              <Form.Group className="mb-3" controlId="Email">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  required
                  value={email}
                  onChange={e => {
                    setEmail(e.target.value);
                  }}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="Password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  required
                  value={password}
                  onChange={e => {
                    setPassword(e.target.value);
                  }}
                />
              </Form.Group>

              {isActive ? (
                <Button variant="success" type="submit" id="submitBtn" block>
                  Login
                </Button>
              ) : (
                <Button variant="secondary" type="submit" id="submitBtn" disabled block>
                  Login
                </Button>
              )}
            </Form>
          )}
        </Col>
      </Row>
    </Container>
  );
	
}