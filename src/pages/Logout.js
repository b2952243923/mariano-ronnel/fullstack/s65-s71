import React, { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {
  const { unsetUser, setUser } = useContext(UserContext);

  // This invokes the unsetUser function from App.js to clear the data/token from the local storage.
  // This will result in the value undefined.
  unsetUser();

  useEffect(() => {
    // Set the user state back to the original value
    setUser({
      id: null,
      isAdmin: null,
    });

    // Reload the website after a small delay (to ensure the state update is completed)
    setTimeout(() => {
      window.location.reload();
    }, 1000);
  }, [setUser]);

  return <Navigate to="/login" />;
}
