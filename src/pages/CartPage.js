import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Cart from '../components/Cart';

const CartPage = ({ user }) => {
  console.log('User:', user);
  const [cartItems, setCartItems] = useState([]);

  const fetchCartItems = (userId) => {
    if (userId) {
      return fetch(`${process.env.REACT_APP_API_URL}/add-to-cart/getCartSubtotal`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log('Data received from API:', data);
          return data.itemsSubtotal;
        })
        .catch((error) => {
          console.error('Error fetching cart items:', error);
          return [];
        });
    }
    return Promise.resolve([]); // Return an empty array as cart items when userId is undefined
  };

  const removeFromCart = (productId) => {
    // Make a DELETE request to remove the item from the cart
    fetch(`${process.env.REACT_APP_API_URL}/add-to-cart/removeProduct`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.message) {
          // Item removed from cart successfully
          fetchCartItems(user?.id).then((items) => {
            setCartItems(items);
          });
        } else {
          // Error occurred while removing from cart
          console.error('Error removing from cart:', data.error);
        }
      })
      .catch((error) => {
        console.error('Error removing from cart:', error);
      });
  };

  const updateQuantity = (productId, newQuantity) => {
    // Make a PUT request to update the item quantity in the cart
    fetch(`${process.env.REACT_APP_API_URL}/add-to-cart/changeQuantity/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        quantity: newQuantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.message) {
          // Quantity updated successfully
          fetchCartItems(user?.id).then((items) => {
            setCartItems(items);
          });
        } else {
          // Error occurred while updating quantity
          console.error('Error updating quantity:', data.error);
        }
      })
      .catch((error) => {
        console.error('Error updating quantity:', error);
      });
  };

  useEffect(() => {
    fetchCartItems(user?.id).then((items) => {
      setCartItems(items);
    });
  }, [user?.id]);

  console.log('Cart Items:', cartItems); // Log the cart items to check their contents

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <h1>Cart Page</h1>
          <Cart cartItems={cartItems} removeFromCart={removeFromCart} updateQuantity={updateQuantity} />
        </Col>
      </Row>
    </Container>
  );
};

export default CartPage;
