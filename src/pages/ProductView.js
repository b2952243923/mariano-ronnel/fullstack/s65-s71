import React, { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import './ProductView.css'; // Import custom CSS for the component
import Cart from '../components/Cart'; // Import the Cart component

export default function ProductView({ user }) {
  const { productId } = useParams();
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [imgUrl, setImgUrl] = useState('');
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);

  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setImgUrl(data.imgUrl);
      });
  }, [productId]);

  const handleQuantityChange = (event) => {
    setQuantity(parseInt(event.target.value, 10)); // Parse the input value to an integer
  };

  const handleAddToCart = () => {
    // Make a POST request to your backend API to add the product to the cart
    fetch(`${process.env.REACT_APP_API_URL}/add-to-cart/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.message) {
          // Product added to cart successfully
          Swal.fire({
            title: 'Product Added to Cart',
            icon: 'success',
            text: 'Product has been added to your cart.',
          });
        } else {
          // Error occurred while adding to cart
          Swal.fire({
            title: 'Error',
            icon: 'error',
            text: 'An error occurred while adding the product to the cart.',
          });
        }
      })
      .catch((error) => {
        console.error('Error adding to cart:', error);
      });
  };

  const order = () => {
    // Check if the user is logged in
    if (!user.id) {
      Swal.fire({
        title: 'Not Logged In',
        icon: 'error',
        text: 'Please log in to place an order.',
      });
      return;
    }

    // If the user is logged in, proceed with the order
    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        userId: user.id,
        products: [{ productId: productId, quantity: quantity }], // Send the selected product and its quantity
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Successfully Ordered',
            icon: 'success',
            text: 'You have successfully placed an order.',
          });

          navigate('/product');
        } else {
          Swal.fire({
            title: 'Something Went Wrong',
            icon: 'error',
            text: 'Please try again.',
          });
        }
      });
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              {/* Display the product image */}
              {imgUrl && <img src={imgUrl} alt={name} className="product-image" />}
              <Card.Title className="mt-3">{name}</Card.Title>
              <Card.Subtitle className="pt-3">Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Card.Subtitle>Quantity:</Card.Subtitle>
              <Card.Text>
                <input
                  type="number"
                  min="1"
                  value={quantity}
                  onChange={handleQuantityChange}
                />
              </Card.Text>
        
              {user.id && (
                <>
                  <Button className='mx-5' variant="success" onClick={order}>
                    Order
                  </Button>
                  <Button variant="success" onClick={handleAddToCart}>
                    Add to Cart
                  </Button>
                </>
              )}
              {!user.id && (
                <Button as={Link} to="/login" variant="danger">
                  Log in to Order
                </Button>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {/* Render the Cart component passing the cartItems, removeFromCart, and updateQuantity props */}
      <Cart cartItems={cartItems} setCartItems={setCartItems} />
    </Container>
  );
}
